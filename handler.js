'use strict';

/**
 *  this function is responsible by interacting with RDS * 
 *  receives param (event) containing the query data
 * 
 *  insert param syntax
 *  {
 *    type: "insert",
 *    table: string,
 *    values: 
 *        [{
 *          "column1": "string" || number,
 *          "column2": "string" || number,
 *          ...
 *        }, {...}, ...]
 *  }
 * 
 *  update param syntax
 *  {
 *    type: "update",
 *    table: string,
 *    set: {"column": "string" || number, ...},
 *    where: [{
 *            "column":   "string",             // unlike insert this string must be filled with column's name
 *            "value":    "string" || number,
 *            "logical":  "AND" || "OR"
 *           }, ...]
 *  }
 * 
 *  select param syntax
 *  {
 *    type: "select",
 *    table: string,
 *    columns: ["column1", "column2", ...],
 *    where: [{
 *            "column":   "string",             // unlike insert this string must be filled with column's name
 *            "value":    "string" || number,
 *            "logical":  "AND" || "OR"
 *           }, ...]
 *  }
 * 
 */

const Pool = require('pg-pool')
const pool = new Pool({
    host: 'lucksapp.c5zrtbq90qvi.us-east-1.rds.amazonaws.com',
    database: 'lucksapp',
    user: 'lucksapp',
    password: 'servico_de_mineiracao_distribuida_com_pagamento_sorteado',
    port: 5432,
    max: 1,
    min: 0,
    idleTimeoutMillis: 100000,
    connectionTimeoutMillis: 1000
});

module.exports.db_handler = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  void async function(){
    let conn;
    try {
      conn = await pool.connect();
      let query;

        switch(event.type){
            case "insert":
                query = buildInsertQuery(event);                
                break;
            case "update":
                query = buildUpdateQuery(event);
                break;
            case "select":
                query = buildSelectQuery(event);
                break;
        }

        try {
          console.log(query);
          let res = await conn.query(query);
          callback(null, res);
        } catch (e){
          callback(null, e);
        }
    } catch (e){
      callback(null, e);
    } finally {
      conn.release();
    }
  }();
};

const buildConditions = (query, payload) => {
  let i = 0;
  payload.where.forEach((condition) => {
    if (i > 0){
      query = query + " " + condition.logical + " ";
    }
    if (condition.arbitrary){
      query = query + condition.arbitrary + " ";
      i++;
    } else {
      query = query + condition.column + " = ";
      if (condition.value.constructor === String){
        query = query + "'";
      }
      query = query + condition.value;
      if (condition.value.constructor === String){
        query = query + "'";
      }
      query = query + " ";   
      i++;
    }
  });
  query = query.slice(0,-1) + ";";

  return query;
}

const buildSelectQuery = (payload) => {
  if (payload.where.constructor === Object){
    payload.where = [payload.where];
  }

  // fill returning columns
  let query = "select ";

  payload.columns.forEach((column) => {
    query = query + column + ", ";
  });
  query = query.slice(0, -2) + " from " + payload.table + " where ";

  // select column1, column2 from table

  // fill conditions
  query = buildConditions(query, payload);

  return query;
}

const buildUpdateQuery = (payload) => {
  if (payload.where.constructor === Object){
    payload.where = [payload.where];
  }

  // fill table
  let query = "update " + payload.table + " SET ";
  
  // fill new values
  Object.keys(payload.set).forEach((k) => {
    if (payload.set[k].constructor === String){
      query = query + k + " = '" + payload.set[k] + "', ";
    } else {
      query = query + k + " = " + payload.set[k] + ", ";
    }
  });
  query = query.slice(0, -2) + " WHERE ";

  // update table set column1 = "a", column2 = 1, ... WHERE   
  
  // fill conditions
  query = buildConditions(query, payload);

  return query;
}

const buildInsertQuery = (payload) => {
  if (payload.values.constructor === Object){
    payload.values = [payload.values];
  }

  // fill table and columns
  let query = "insert into " + payload.table;
  query = query + " (";
  Object.keys(payload.values[0]).forEach((k) => {
    query = query + k + ", ";
  });
  query = query.slice(0, -2) + ") VALUES ";

  // insert into tables (column1, column2, ...) VALUES 
  
  // fill values
  payload.values.forEach((row)=>{
    query = query + " (";
    Object.keys(row).forEach((column)=>{
      if (row[column].constructor === String){
        query = query + "'" + row[column] + "', ";
      } else {
        query = query + row[column] + ", ";
      }
    });
    query = query.slice(0, -2) + "),";
  });
  query = query.slice(0, -1) + ";";

  // insert into tables (column1, column2, ...) VALUES (1, 'a', ...), ...;
  
  return query;
}
